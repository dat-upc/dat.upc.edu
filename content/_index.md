---
title: "Delegació d'Alumnes de Telecos"
featured_image: "/images/campus-nord.jpg"
---
{{< custom-main-page >}}
La Delegació d’Alumnes de Telecomunicacions és l’òrgan de coordinació dels representants de l’estudiantat dels graus i màsters universitaris dins l’àmbit de l’Escola Tècnica Superior d’Enginyeria de Telecomunicacions de Barcelona (ETSETB). Ens dediquem a ajudar als estudiants i a vetllar pels seus drets i interessos al llarg de la seva estada a l’Escola.

Per tal d’aconseguir-ho, ens posem a la seva disposició per resoldre qualsevol dubte, queixa o problema que puguin tenir i els representem en els diversos òrgans de govern de l’Escola que inclouen: la junta d’escola i la comissió permanent de l’escola. També els oferim un servei de consulta d’apunts i exàmens de cursos anteriors, que poden utilitzar descarregant-los d’aquesta web.

