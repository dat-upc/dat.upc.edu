---
title: "Delegats"
draft: false
---

La principal funció del delegat és fer de vincle entre el professorat i l'alumnat del vostre grup. És, amb diferència, la funció més important i útil, tant pels estudiants com pel professorat. Si heu sigut escollits com a delegats, convindria que tinguéssiu ben clar quines són les feines que pertoca fer a un delegat i quines no.  

‏‏‎‎   

+ **Canvi de parcial a mitjan curs:** El professor ha de respectar el que va dir el primer dia de curs. En qualsevol cas, per canviar els parcials (si per exemple coincideix amb un altre parcial), s'ha de parlar amb tota la classe i arribar a un dia que afavoreixi a totes les parts.
+ **Dos parcials el mateix dia:** Els delegats ho parlen amb el professor i demanen dates alternatives. Si són molt pocs alumnes en un grup o en un laboratori els qui demanen canvi de parcial, ho parlen directament amb el professor.
+ **Nota mínima de l'examen final:** No es pot imposar cap situació d'avaluació on un acte avaluable sigui condicionat per la nota d'un altre acte avaluable. En casos que es vulgui aplicar una mitjana geomètrica, la nota mínima per aplicar-la és un 1.
+ **Exàmens sorpresa:** El professor hauria d'avisar el primer dia de classe de la possibilitat d'aquests exàmens, no s'acostumen a donar, però el professor ha hagut d'avisar i explicar el funcionament i format d'aquests i el percentatge de nota que tenen.
+ **Parcials, laboratoris, o classes fora d'horari lectiu:** Els dimecres d'11-12 i de 17-20 és horari no lectiu. Si es volgués fer alguna classe en aquest horari, s'hauria de demanar permís al Cap d'Estudis i la classe hauria d'estar d'acord. Si a algú no li fos possible assistir-hi, el professor li haurà de proporcionar opcions per realitzar el que s'hagi impartit en aquella sessió.
+ **Problemes amb els subgrups:** Si no podeu anar al grup o subgrup al qual esteu matriculats heu de parlar-ho amb el professor per tal d'intentar trobar una solució, recordeu que sense una modificació de matrícula, el que heu matriculat és el que preval i el que és oficial.
+ **Professors que arriben tard:** Els delegats li han de comentar el problema i suggerir-li puntualitat. Un professor no pot endarrerir el ritme de classe.
+ **Professors que no va a classe:** Els delegats li han de comentar el problema i s'ha de tenir la possibilitat d'un substitut o trobar una solució.
+ **Percentatge molt baix d'aprovats en un parcial:** Comentar-li al professor la possibilitat de repetir l'examen o fer alguna feina complementaria. Seria convenient fer una comparativa amb el coordinador de l'assignatura de com han anat les notes en els altres grups.
+ **Professors que no segueix el temari:** Assegurar-se que dóna el temari que entra a l'examen final. També existeix la possibilitat que un professor faci el seu propi.
+ **Incompatibilitat professors de teoria i problemes:** Parlar amb els dos professors i comentar-los que es posin d'acord entre ells per anar al mateix ritme. És important que no es contradiguin.  


Resulta **força convenient** que els delegats comenteu **qualsevol problema o incidència** amb la DAT i que vingueu a les reunions que es realitzen. D'aquesta manera estarem informats de la situació i podrem parlar amb qui correspongui o recomanar-vos la millor manera d'actuar. Us agrairíem que ens féssiu arribar la vostra opinió i possibles dubtes, a més a més de la vostra col·laboració, en la mesura que pugueu, perquè puguem millorar el funcionament de la Delegació d'Alumnes.


## Els delegats actuals
{{< llista-delegats >}}
