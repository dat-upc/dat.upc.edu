---
title: "Secretaria"
draft: false 
---

El **Secretari o Secretària** és l’encarregat de la correcta documentació de la feina que realitza la Delegació. Redacta actes a les reunions internes i s’encarrega del correu electrònic de la Delegació.

