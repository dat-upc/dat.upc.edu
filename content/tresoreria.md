---
title: "Tresoreria"
draft: false 
---

El rol del **Tresorer o de la Tresorera** és la gestió dels costos econòmics que comporta la tasca de la Delegació. Pagament de material, gestió de tiquets, compres per a esdeveniments, etc. És escollit pel Delegat de Centre després que aquest sigui escollit.

