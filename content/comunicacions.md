---
title: "Responsable de comunicacions"
draft: false 
---

**El o la responsable de comunicacions** s’encarrega del contacte directe amb els estudiants a través de les xarxes socials. S’encarrega també de la difusió dels actes de la Delegació i col·laboracions per a actes i esdeveniments amb els que la delegació pugui solidaritzar, així com la promoció d’activitats que realitzem a la delegació. Rep una formació per part del seu antecessor durant un quadrimestre, i per tal de poder donar una resposta ràpida als estudiants, està recolzada per una beca d’aprenentatge de 5 hores a la setmana.


