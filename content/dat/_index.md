---
title: "La DAT"
featured_image: "/images/campus-nord2.jpg"
---

# DELEGACIÓ D’ALUMNES DE TELECOMUNICACIONS


Ens podràs trobar sempre en aquesta ubicació:

 {{< dat >}}

Edifici Omega – Despatx 002  
Campus Nord UPC  
C/ Jordi Girona 1-3, 08034 Barcelona

Correu electrònic: [dat@dat.upc.edu](mailto:dat@dat.upc.edu)  
Instagram: [@dat_upc](https://www.instagram.com/dat_upc/)  
Telèfon de contacte: +34 93 413 76 70  
Recorda que si tens un dubte o queixa relacionat amb l’Escola també ens el pots fer arribar a través d’ aquest [formulari](https://dat.upc.edu/queixes-i-dubtes)

---

