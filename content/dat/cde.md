---
title: "El CDE"
featured_image: ""
summary: "El Consell De l'Estudiant"
omit_header_text: true
draft: false
logo: "/images/logos/logoCDE.png"
---



La DAT forma part de Consell de l’Estudiantat de la UPC, aquest òrgan està integrat per totes les delegacions de tots els centres de la UPC, és l’ens estudiantil que representa tots els estudiants i totes les estudiantes de la UPC, i s’encarrega de la defensa dels drets de l’estudiantat. Mes informació a la seva web: [CDE](http://cde.upc.edu).

Actualment la DAT participa en totes les seves comissions (Comissió de Tresoreria, Comissió de Reglament, Comissió de Comunicacions, ...) i activament en els Plens i altres propostes.

---
{{< cde >}}
