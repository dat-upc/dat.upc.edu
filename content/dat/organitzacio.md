---
title: "La DAT"
featured_image: ""
summary: "Com ens organitzem a la delegació"
omit_header_text: true
draft: false
---



 {{< la-dat >}}


---
La delegació s’organitza internament mitjançant dos òrgans col·legiats:

#### Ple de la DAT
El cual el formen: La delegació del centre, tresoreria, secretaria, sotdelegació de centre i delegats i delegades.


#### Consell de la DAT
El cual el formen: La delegació del centre, tresoreria, secretaria, sotdelegació de centre i convidats.

---

