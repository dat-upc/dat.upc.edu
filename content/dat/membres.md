---
title: "Membres"
draft: false 
summary: "Els que formem part de la delegació"
omit_header_text: true

---

## Equip coordinador
{{< coordinadors >}}
## Sotsdelegades de centre
{{< sots-delegats>}}
## Hall of fame
{{< hall-of-fame >}}
