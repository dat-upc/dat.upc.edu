---
title: "Delegat de Centre"
draft: false 
---

El **Delegat o Delegada de Centre** és el màxim responsable de la delegació. Ha estat escollit pel Ple d’aquesta en votació secreta. La seva responsabilitat és que la Delegació segueixi el rumb correcte, vetllar perquè així sigui. És membre nat a òrgans de govern de l’escola i la UPC.


