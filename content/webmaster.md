---
title: "Webmaster"
draft: false 
---

**El o la webmaster** és l’encarregat del bon funcionament de la web de la DAT. S’encarrega de l’implementació de noves eines virtuals per l’estudiantat i facilita l’arribada d’informació al mateix de manera clara i intuitiva. Ha rebut una formació durant un quadrimestre pel webmaster anterior, i com que aquesta tasca requereix hores de feina i dedicació, està recolzada per una beca d’aprenentatge de 5 hores a la setmana.

