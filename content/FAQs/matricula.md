---
title: "Matrícula"
summary: "Si has arribat fins aquí, vol dir que busques més informació sobre la matriculació"
featured_image: ''
omit_header_text: true
draft: false
---
{{< ample >}}

# Quan es fa la matrícula d’un estudiant no nou?

En l'ETSETB s'ofereix doble docència (és dona docència de totes les assignatures **dues vegades a l'any**) i és per això que ens matriculem dues vegades. La matrícula pel **quadrimestre de tardor** es duu a terme al **juliol** i la matrícula del **quadrimestre de primavera** a principis de **febrer**. Per tal de poder gestionar tots els estudiants i que aquests puguin fer totes les assignatures que desitgen dues setmanes abans es fa una prematrícula. Aquesta **és obligatòria i orientativa**, és a dir, si no es fa, es perd prioritat en l'ordre de matrícula i això pot provocar no poder triar el grup que l'estudiant desitgi i és orientativa en el sentit que no es trien grups i les assignatures prematriculades no són definitives: si en el moment de la matrícula no es volen cursar, no és obligatori seleccionar–les.  

En el cas del grau de GREELEC, les assignatures només es cursen un cop per curs.

# On es fa la matrícula d'un estudiant no nou?

La matrícula es pot fer realitzar en el portal de **[e-secretaria](https://prisma-nou.upc.edu/apl/home_estudiants.php?idioma=1)**. A l'esquerra es troba un menú on apareix l'apartat de "Matrícula". Aquest es divideix en més opcions, entre elles "Condicions de Matrícula" i "Matrícula". En el primer, uns dies abans, podrem consultar el dia i l'hora exacte en les que podrem accedir a matricular-nos, a més a més dels descomptes que tenim actualment. El segon només serà visible a partir de la data mencionada anteriorment.

# De quantes assignatures em puc matricular?

En la primera matrícula (estudiants de nou accés) has de matricular **com a mínim totes les assignatures del primer quadrimestre** (5 assignatures – 30 crèdits) si es vol cursar la via total. També es pot triar la via parcial, matriculant 3 assignatures (18 crèdits).

Durant la fase inicial no hi ha límit en el nombre de crèdits matriculables. No obstant això, l'estudiant no es pot matricular d'assignatures de cursos superiors fins que només tingui pendents tres o menys assignatures d'aquesta primera fase. En aquest supòsit, podrà matricular com a màxim 4 assignatures del tercer quadrimestre, o 3 si no és el primer cop que s'acull a l'excepció.

Un cop superada la fase inicial (primer curs) i en condicions normals, es poden matricular com a màxim 36 crèdits (6 assignatures), sense tenir en compte crèdits convalidats o reconeguts (per exemple per a la representació dels estudiants o per activitats esportives).

Si has suspès alguna assignatura durant els quadrimestres anteriors, pot ser que se t'apliqui alguna restricció especial.

# Quants crèdits haig d'aprovar per complir la normativa de permanència?

#### Durant el primer curs (Fase Inicial): 
La normativa de l'ETSETB exigeix aprovar tots els crèdits del primer curs durant **els primers dos anys**. A més, el primer any **s'han d'aprovar com a mínim 12 crèdits** (2 assignatures de 6 crèdits). En el cas que no es compleixi algun d'aquests dos requisits, l'estudiant queda exclòs dels estudis, tot i que es pot demanar a la direcció de l'escola que excepcionalment es concedeixin fins a dos quadrimestres extra. Per demanar aquest quadrimestre extra s'ha de fer a través de [e-secretaria](http://esecretaria.upc.edu/), o bé en l'apartat específic de "Tràmits de Permanència" o bé, si no es troba l'opció anterior, a través d'"Altres tràmits".

#### Un cop superada la Fase Inicial: 
Un cop superada la Fase Inicial, al final de cada quadrimestre **es calcula el paràmetre de resultats** acadèmics de cada estudiant, que és el quocient de crèdits superats sobre el total de matriculats.

Si un estudiant té un paràmetre de resultats inferior a 0,5 durant dos quadrimestres consecutius, se li limitarà la matrícula del següent. I si té un paràmetre de resultats inferior a 0,3 durant tres quadrimestres consecutius, serà desvinculat automàticament dels estudis per a un període màxim de dos anys. L'estudiant desvinculat, amb l'autorització prèvia de l'ETSETB, pot reiniciar els estudis un cop transcorregut el període de desvinculació resolt.

# Què són els pre-requisits i pre-co-requisits?

Els **pre-requisits** d'una determinada assignatura són assignatures que un estudiant ha **d'haver superat prèviament per poder-se'n matricular**. Els **pre-co-requisits**, en canvi, només exigeixen que aquestes assignatures **es cursin simultàniament.**

A efectes de pre i pre-co-requisits, computen com a superades aquelles assignatures amb una nota superior o igual a 4.

La taula en la qual s'especifiquen els pre-requisits i pre-co-requisits de cada assignatura del greu de GRETST es troben a: [Taula de pre-requisits i pre-co-requisits](http://etsetb.upc.edu/es/shared/normatives/taula_pre_cor_assignatures_grests_basic.pdf)

# Què és l'alpha?

L'alpha és el **paràmetre que mesura el rendiment acadèmic** dels últims dos quadrimestres cursats, és a dir, la relació entre els crèdits superats i els matriculats. Si un estudiant aprova tots els crèdits matriculats la seva alpha és d'1, si n'aprova la meitat la seva alpha és de 0,5…

# Com es determina l’ordre de matrícula dels estudiants?

Es tenen en compte 4 factors, per ordre decreixent d’importància:

* Nombre de crèdits aprovats en els últims dos quadrimestres.  
* Paràmetre de rendiment acadèmic (alpha).  
* Nota mitjana ponderada dels tres últims quadrimestres.  
* Ordre alfabètic.

# Si he repetit una assignatura, com afecta al preu de l'assignatura si la torno a matricular?

El preu del crèdit d'una assignatura augmenta en funció del nombre de vegades
que s'ha suspès:

Matrícula|Preu|Multiplicador
:-------:|:--:|:-----------:
Primera|27,67|x1
Segona|34,17|x1,2
Tercera|74,02|x2,6
Quarta o més|102,52|x3,6

# A quines beques puc optar per a ajudar-me a pagar la matrícula?

Bàsicament, existeixen dos tipus de beques: les del ministeri i les de la generalitat:

Les beques del ministeri tenen en compte diferents requisits acadèmics i de renda i patrimoni. Més informació a:

**<https://www.upc.edu/sga/ca/Beques/BecaGralMECD>**

Les beques Equitat, adreçades específicament a les universitats catalanes, que permeten obtenir una reducció d'entre el 10 i el 50% del preu de la matrícula, segons el nivell de renda familiar del sol·licitant. Més informació a:

**<https://www.upc.edu/sga/ca/Beques/BequesAgaur/BecaEquitat>**

Les famílies nombroses de categoria general i especial també tenen una reducció del preu de la matrícula del 50% i el 100%, respectivament.

Podeu trobar informació sobre la resta de beques i ajuts a:

**<https://www.upc.edu/sga/ca/Beques>**

# Com puc organitzar-me els horaris a l’hora de fer la matrícula?

Per a organitzar-te fàcilment, et recomanem que utilitzis el planificador d’horaris que posa a disposició l’escola per als alumnes, que és de gran utilitat:

**<https://infoteleco.upc.edu/documents/gdqpgt75.html>**

# Què passa si m'he matriculat de dues assignatures que se superposen en l'horari?

Si t'has matriculat d'una assignatura representa que **has de poder assistir a totes les hores** ja que es donen diferents opcions d'horaris amb l'objectiu que això no passi. Si no tens altra opció, has de ser conscient que si coincideixen dos exàmens parcials o qualsevol altre acte d'avaluació no podràs justificar l'absència. Recomanem mirar bé els calendaris de finals, disponibles a l'apartat Curs Actual de la web de l'escola, per comprovar que no se superposin els exàmens i ser molt prudents amb aquesta situació, ja que **no es podran moure exàmens parcials**.

# Què són els seminaris, quan es fan i quan m'hi haig de matricular?

Els seminaris són **cursets normalment de 2 crèdits** ECTS (una assignatura ordinària és de 6 crèdits) que es cursen de forma optativa durant el curs, durant el mes de febrer (entre el primer i segon quadrimestre) o bé durant el mes de juny, un cop finalitzats els exàmens.

Normalment, s'han de matricular junt amb la matrícula ordinària del setembre o febrer, però se solen obrir les inscripcions un segon cop durant el quadrimestre (l'escola avisa amb un correu).
