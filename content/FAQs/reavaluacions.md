---
title: "Reavaluacions"
summary: "Si has arribat fins aquí és perquè tens dubtes sobre les reavaluacions, a continuació trobaràs un resum acurat sobre tot perquè puguis aclarir els teus dubtes. Si encara te'n queden, no dubtis en venir al nostre despatx!"
featured_image: ''
omit_header_text: true
draft: false
---
{{< ample >}}

# Període de reavaluacions
Hi haurà **un únic període** de reavaluacions per als dos quadrimestres. Aquest tindrà lloc a la **primera quinzena de juliol.**  


# Assignatures amb reavaluació
Els estudiants matriculats en el qualsevol dels graus de l'escola podran optar a reavaluació de les assignatures del bloc comú: primer, segon i el 3A.

Excepcionalment, si una assignatura de menció, optativa o de cursos superiors al 3B té un nombre de suspensos superior al 35%, el professor o el cap d'estudis podrà proposar de fer un examen de reavaluació extraordinari.
No són reavaluaves les assignatures que no s'avaluïn normalment mitjançant exàmens (per exemple: ENTIC, PBE o PAE).  


# Modificació de les qualificacions
Només es modificarà la nota de l’examen final en el cas que sigui inferior a la nota de la reavaluació. 

Es manté la nota més alta entre l’examen final i la reavaluació.
L’examen de reavaluació tindrà el **mateix pes en la ponderació** que l’examen final. Les assignatures amb laboratori o amb matèries no avaluables amb examen **mantindran els seus percentatges.**  


# Condicions per presentar-se i matrícula
Els alumnes que estiguin cursant el quadrimestre de tardor només podran optar a la reavaluació de **dues assignatures** amb una nota **igual o superior a 3.** Per fer-ho, hauran de matricular-se al quadrimestre de primavera al grup REAVA de l'assignatura. Aquesta **matrícula és sense cost** i dóna dret només a realitzar l'examen extraordinari que tindrà lloc al mes de juliol. Els estudiants podran optar també a la matrícula ordinària de l'assignatura amb el cost que comporta la realització d'una segona matrícula.  


Els alumnes que estiguin cursant el **quadrimestre de primavera** podran optar a la reavaluació de **totes les assignatures** sense cap tipus de matrícula.
**No hi ha límit de places** per la realització de l'examen de reavaluació.
Si la nota obtinguda a l'examen de reavaluació és més baixa que la que vas treure a la matrícula ordinària, **sempre se't quedarà la nota més alta.**  


# Classes de preparació
**No es garanteix** que hi hagi classes de preparació de totes les assignatures amb reavaluació. Abans de la matrícula de febrer s'informarà de quines assignatures tenen classes de preparació, i els seus horaris corresponents.
En el cas d'haver-hi places limitades per als cursos intensius o cursos de repàs tindran prioritat els alumnes amb notes més altes.

Si una assignatura té curs de repàs al quadrimestre de primavera i curs intensiu al juny del mateix quadrimestre, tindran prioritat per als cursos intensius els alumnes matriculats al febrer i els que no hagin pogut participar en els cursos de repàs per falta de places. Aquests seran ordenats segons les qualificacions més altes.
Aquests cursos en qualsevol cas **són gratuïts com la reavaluació.**

