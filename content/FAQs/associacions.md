---
title: "Associacions"
summary: "Què son les associacions i quines hi ha a l'ETSETB?\n Què em pot aportar formar part d’una associació?"
featured_image: ''
omit_header_text: true
draft: false
---

# Què son les associacions i quines hi ha a l’ETSETB?

Són agrupacions d’estudiants que tenen com a objectiu representar els estudiants i/o fomentar la vida universitària. En el campus nord les més importants són:

{{< associacions >}}
<!---
[AESS (Aerospace and Electronics Systems Society)](https://aess.upc.es/)  

[AUCCOP (Associació d’Universitaris per a laCooperació)](https://aucoop.upc.edu/ca)  

[Distorsió – La revista dels estudiants de TelecomBCN](https://www.facebook.com/DistorsioTelecos)  

[FòrumTIC](http://forumtic.upc.edu/es/bienvenida/)  

[IEEE](http://ieee.upc.es/)  

[A.C. Telecogresca](https://www.telecogresca.com/)  

[Cosmic Research](http://cosmicresearch.org/)  

[DAT (Delegació d’Alumnes de Telecos)](http://dat.upc.edu/)
-->
‏‏‎‎ ‏‏‎‏‏‎‎ ‏‏‎‎  
Podeu veure una llista de totes les associacions a: [Associacions](https://etsetb.upc.edu/ca/escola/associacions)

# Què em pot aportar formar part d’una associació?

Sobretot durant el segon i tercer curs, et pot permetre, a més de conèixer molta gent nova, de participar intensivament en activitats del campus. Més concretament, formar part d’associacions com la DAT us pot permetre conèixer amb detall com funciona una entitat pública com la ETSETB i la UPC i participar activament en la presa de decisions dins d’aquestes.
