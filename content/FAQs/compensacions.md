---
title: "Compensacions"
summary: "Si has arribat fins aquí, vol dir que busques més informació sobre les compensacions, esperem que això t'ajudi."
featured_image: ''
omit_header_text: true
draft: false
---
{{< ample >}}

# Què son les compensacions?

Al llarg de la carrera existeix la possibilitat de compensar, es a dir “d’aprovar” una assignatura si aquesta esta suspesa, però amb certes restriccions. Només es poden compensar dues assignatures al llarg de tota la carrera: una de la fase inicial (1r curs) i l’altre de la resta. Es pot compensar l’assignatura si la nota final d’aquesta és ≥4 i la mitjana de les altres assignatures es ≥5. Les compensacions es duen a terme a les Comissions d’Avaluació, on els estudiants estan representats a través de la DAT, que es reuneix al Febrer i al Juliol. Si es compleixen tots els requisits la compensació és fa de manera automàtica.

Exemple:
* He suspès CAVEC (fase inicial) amb un 4,3 però la mitjana de la resta d’assignatures de 1r es un 6. En aquest cas la compensació és fa de manera automàtica.
* He suspès ICOM (fase específica o menció) amb un 4 però la mitjana de la resta d’assignatures de la carrera és un 6,3. En aquest cas la compensació es dura a terme una vegada acabades TOTES les assignatures de la carrera.

### Quan es compensa? 
La compensació es realitza quan heu cursat totes les assignatures del bloc curricular corresponent.

### Què haig de fer?
Si compleixes les condicions abans esmentades el procés és automàtic. Si encara així vols estar segur que quan se celebrin les comissions avaluadores el representant de la DAT miri el teu cas, hauràs d'estar atent a les xarxes, ja que mitjançant aquestes, enviarem un formulari.

### No compleixo les condicions, què faig?
Potser es dóna el cas que et queda una assignatura amb una nota molt propera a 4, però no entra dins, en aquest cas el procés no és automàtic, però podem intentar defensar el teu cas, si tens una situació similar o creus que s'hauria de revisar el teu cas, fes dues coses:

1. **IMPRESCINDIBLE!** Fes una sol·licitud formal a la comissió avaluadora del teu grau: [Sol·licituds a les Comissions d’Avaluació Curricular](https://www.etsetb.upc.edu/ca/els-serveis/secretaria-oberta/procediments-i-tramits/comissions-davaluacio-curricular-cac-assignatures-compensables-graus#section-1).

2. Omple el formulari que farem arribar a finals de quadrimestre i d'aquesta manera podrem estar informats del teu cas i defensar-lo a la comissió corresponent.


# Que són els blocs curriculars
El grau **GRETST** s'estructura en tres blocs curriculars:
- Fase Inicial constituïda pels 60 crèdits ECTS de les 10 assignatures del 1r curs del pla d'estudis.
- Fase de Menció constituïda pels 162 crèdits de la resta d'assignatures del pla d'estudis.
- El Treball de Fi de Grau (TFG) de 18 crèdits, és la darrera fase i no és objecte d'una avaluació curricular com les anteriors.  

El grau **GREELEC** s'estructura en tres blocs curriculars:
+ Fase Inicial constituïda pels 60 crèdits ECTS de les 10 assignatures del 1r curs del pla d'estudis.
+ Fase Troncal constituïda pels 162 crèdits de la resta d'assignatures del pla d'estudis.
+ El Treball de Fi de Grau (TFG) de 18 crèdits, és la darrera fase i no és objecte d'una avaluació curricular com les anteriors.  

El grau **GEF** s'estructura en quatre blocs curriculars:
* Fase Inicial constituïda pels 60 crèdits ECTS de les 10 assignatures del 1r curs del pla d'estudis.
* Fase de 2n constituïda pels 60 crèdits ECTS de les 10 assignatures del 2n curs del pla d'estudis.
* Fase de 3r constituïda pels 60 crèdits ECTS de les 10 assignatures del 3r curs del pla d'estudis.
* Els 30 crèdits optatius i el Treball de Fi de Grau (TFG) de 30 crèdits, és la darrera fase i no és objecte d'una avaluació curricular com les anteriors.
