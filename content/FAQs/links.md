---
title: "Links d'interès"
summary: "Aqui pots trobar tots els enllaços que et puguin resultar útils. "
featured_image: ''
omit_header_text: true
draft: false
---
{{< ample >}}
## Calendaris Lectius ETSETB

* [Calendaris Lectius 2020-21](https://telecos.upc.edu/ca/shared/curs-actual/calendari-lectiu)

## Grau en Enginyeria de Tecnologies i Sistemes de Telecomunicació
* [Normativa del Grau](https://telecos.upc.edu/ca/shared/acord-cp2022-07-11-saprova-modificacio-normativa-academica-gretst.pdf)

* [Calendari exàmens Finals](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/calendari-dexamens)

* [Horari de classes](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/horaris-de-classe)

* [Planificador d'horaris](http://infoteleco.upc.edu/documents/gdqpgt75.html)

* [Professorat per assignatura](https://telecos.upc.edu/ca/estudis/curs-actual/professorat-responsables-dassignatura-i-coordinadors-de-graus/copy_of_professorat-assignat-a-les-assignatures-i-idioma-dimparticio)

## Grau en Enginyeria Electrònica de Telecomunicació
* [Normativa del Grau](https://telecos.upc.edu/ca/shared/acord-cp2022-07-12-saprova-modificacio-normativa-academica-greelec.pdf)

* [Calendari exàmens Finals](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/calendari-dexamens)

* [Horari de classes](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/horaris-de-classe)

* [Planificador d'horaris](http://infoteleco.upc.edu/documents/gdqpgt75.html)

* [Professorat per assignatura](https://telecos.upc.edu/ca/estudis/curs-actual/professorat-responsables-dassignatura-i-coordinadors-de-graus/copy_of_professorat-assignat-a-les-assignatures-i-idioma-dimparticio)

## Grau en Enginyeria Física
* [Normativa del Grau](https://enginyeriafisica.etsetb.upc.edu/ca/curs-actual/normatives/naef.pdf)

* [Calendari exàmens Finals](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/calendari-dexamens)

* [Horari de classes](https://telecos.upc.edu/ca/estudis/curs-actual/horaris-aules-i-calendaris/horaris-de-classe)

* [Planificador d'horaris](http://infoteleco.upc.edu/documents/gdqpgt75.html)

* [Professorat per assignatura](https://telecos.upc.edu/ca/estudis/curs-actual/professorat-responsables-dassignatura-i-coordinadors-de-graus/copy_of_professorat-assignat-a-les-assignatures-i-idioma-dimparticio)

# Grau en Ciència i Enginyeria de Dades

* [Calendari exàmens Finals](https://www.fib.upc.edu/es/estudios/grados/grado-en-ciencia-e-ingenieria-de-datos/examenes)

* [Horari de classes](https://www.fib.upc.edu/ca/estudis/graus/grau-en-ciencia-i-enginyeria-de-dades/horaris?&a=AP1-GCED_10&a=AP1-GCED_11&a=AP1-GCED_12&class=false&quad=2020Q1)
