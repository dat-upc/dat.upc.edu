---
title: "Exàmens"
draft: false
---

Aquesta és una pàgina provisional, que podeu utilitzar per buscar el material que necessiteu mentre el nostre becari web fa la **nova pàgina de la Delegació** I'm trying ;(. 
## Links amb recursos
* [UPC commons](https://upcommons.upc.edu/handle/2117/135156), on trobareu recursos penjats per la pròpia UPC.
* [Carpeta de drive](https://drive.google.com/drive/folders/1oq0y6AnqRCClgRdW1TgL_hbeUFZd0bCq?usp=sharing), on estaran, de forma temporal, tots els exàmens i apunts del nostre repositori. **HEU D'ENTRAR AMB EL VOSTRE MAIL @estudiantat.upc.edu**
## Penja apunts
En un futur, tindreu habilitada aquesta secció per poder-nos enviar els vostres recursos. Qui sap, potser podreu rebre alguna recompensa...
